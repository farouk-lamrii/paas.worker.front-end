package edu.esipe.i3.ezipflix.frontend.data.configuration;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

@Configuration
@EnableDynamoDBRepositories(basePackages = "edu.esipe.i3.ezipflix.frontend.data.repositories")
public class DynamoDbConfig {

	@Value("${amazonProperties.accessKey}")
	private String Cle;

	@Value("${amazonProperties.secretKey}")
	private String CléSecrete;

	@Value("${amazonProperties.region}")
	private String regionBDD;

	@Value("${amazonProperties.dynamoEndpointUrl}")
	private String DynamondPoint;

	@Bean
	public DynamoDBMapper mapper() {
		return new DynamoDBMapper(amazonDynamoDBConfig());
	}

	public AmazonDynamoDB amazonDynamoDBConfig() {
		return AmazonDynamoDBClientBuilder.standard()
				.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(DynamondPoint, regionBDD))
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(Cle, CléSecrete)))
				.build();
	}
}