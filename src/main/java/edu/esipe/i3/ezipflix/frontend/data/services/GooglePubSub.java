package edu.esipe.i3.ezipflix.frontend.data.services;

import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Service
public class GooglePubSub {
	

	private ProjectTopicName projectTopicName = ProjectTopicName.of("paaslamri", "PAAS");
	private static Publisher publisher = null;

	public GooglePubSub() throws IOException {
		publisher = Publisher.newBuilder(this.projectTopicName).build();
	}

	public static void publishMessage(String message) throws Exception {

	    try {
	      ByteString data = ByteString.copyFromUtf8(message);
	      PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();
	      publisher.publish(pubsubMessage);
	       
	    } finally {
	      if (publisher != null) {
	        publisher.shutdown();
	        publisher.awaitTermination(1, TimeUnit.MINUTES);
	      }
	    }
	  }
}
