package edu.esipe.i3.ezipflix.frontend.data.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class VideoConversionRepository {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    public void save(VideoConversions conversions) {
        dynamoDBMapper.save(conversions);
    }
}
