package edu.esipe.i3.ezipflix.frontend;

/**
 *
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
public class ConversionRequest {

    private String file, bucket ;

    public ConversionRequest() {
    }

    public String getFile() {
        return file;
    }

	public String getBucket() {
		return bucket;
	}

    
    
}
