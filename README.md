################################### Infos sur certains fichiers #################################

Concernant le curl : j'ai délibérément modifier le nom du fichier à convertir. Dans la version de base on était sur un fichier intro.mkv mais ma connexion prenait un peu de temps
pour tout ce qui est download et upload et vu que j'avais besoin de faire pas mal de tests j'ai préféré travailler sur un fichier léger (un txt).

Front-end : il y a un chemin qui est en dur dans videoConversion.java pour aller récupérer un fichier à upload quand ce dernier n'existe pas dans le S3. C'est bien evidemment dans
un cadre scolaire, car sinon nous aurions mis l'adresse d'un serveur dans lequel se trouverai ces fichiers.

Back-end : Il y a également des chemins en dur : le premier a été renseigné dans application.yaml et qui me permet de vérifier si le fichier existe bien en local pour ne pas avoir à 
le retélécharger par exemple.
Le second se trouve dans le fichier de configuration et permet de charger le fichier application.yaml.
La remarque faite du côté Front-end est valable sur le Back-end








################################ Explication choix BDD #############################################

Pour le choix de la base de données, j'avais deux possibilités : Datastore de Google ou DynamoDB d'Amazon
Mon choix s'est orienté en direction de la solution proposée par Amazon car j'ai trouvé la documentation beaucoup plus claire à comprendre
que celle de son concurrent où j'avais l'impression qu'il fallait pousser un peu ses recherche dans la doc pour vraiment la maitriser.
Au delà de l'aspect purement technique, je me suis renseigné sur les modalités de facturations des deux solutions et encore une fois c'était beaucoup plus claire du côté 
d'amazon.En effet ce dernier  propose plusieurs options en fonction de ses besoins alors que Google ne proposait qu'une seule option.