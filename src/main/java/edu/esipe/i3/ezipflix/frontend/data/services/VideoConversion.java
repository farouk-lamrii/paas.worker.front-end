package edu.esipe.i3.ezipflix.frontend.data.services;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import edu.esipe.i3.ezipflix.frontend.ConversionRequest;
import edu.esipe.i3.ezipflix.frontend.ConversionResponse;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import edu.esipe.i3.ezipflix.frontend.data.repositories.VideoConversionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
@Service
public class VideoConversion {

    @Value("${amazonProperties.accessKey}")
    private String Cle;

    @Value("${amazonProperties.secretKey}")
    private String CleSecrete;

    @Value("${amazonProperties.bucketName}")
    private String NomDuBucket;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;


    @Autowired VideoConversionRepository videoConversionRepository;

    GooglePubSub googlePubSub;
    private AmazonS3 amazonS3;
    File fichier=null;

    @PostConstruct
    private void initializeAmazon() {
        AWSCredentials credentials = new BasicAWSCredentials(this.Cle, this.CleSecrete);
         this.amazonS3 = AmazonS3ClientBuilder.standard().withRegion("eu-west-3").withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
    }

    public void IdentificationFichier(final ConversionRequest request, final ConversionResponse response) throws Exception {

        final VideoConversions conversion = new VideoConversions(response.getUuid().toString(), request.getBucket(),request.getFile());

        if (this.detectIfObjectIsInBucket(request.getBucket(),request.getFile())) {
            videoConversionRepository.save(conversion); // Lancement de la sauvegarde dans DynamoDB
        }

        else {
            fichier = new File("C:\\Users\\farou\\Documents\\Giraud\\converter\\video-converter\\"+request.getFile());
            InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(request.getBucket(),String.valueOf(response.getUuid()));
            amazonS3.initiateMultipartUpload(initRequest);

            try {
                amazonS3.putObject(new PutObjectRequest(request.getBucket(), request.getFile(), fichier));
                videoConversionRepository.save(conversion); // Lancement de la sauvegarde dans DynamoDB

            } catch (AmazonServiceException ase) {
            } catch (AmazonClientException ace) {
            }
        }

        this.googlePubSub = new GooglePubSub();
        this.googlePubSub.publishMessage(conversion.toJson()); // Publiction d'un msg dans GooglePubSub

    }

    public Boolean detectIfObjectIsInBucket(String bucketname ,String mys3object){
        ObjectListing objects = amazonS3.listObjects(new ListObjectsRequest().withBucketName(NomDuBucket).withPrefix(mys3object));
        for (S3ObjectSummary objectSummary: objects.getObjectSummaries()) {
            if (objectSummary.getKey().equals(mys3object)) {
                return true;
            }
        }
        return false;

    }
}
